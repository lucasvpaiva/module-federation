const Header = (await import('header/Header')).default;

export default function Home() {
  return (
    <>
      <Header title={"Remote Header"} />
    </>
  );
}
