import React from 'react';

const styles = {
  width: '100%',
  heigth: '80px',
  background: '#433e66',
  color: '#fff',
  fontSize: '48px',
  padding: '10px 20px'
}

const Header = ({ title }) => {
  return <div style={styles}>
    {title}
  </div>
};

export default Header;
