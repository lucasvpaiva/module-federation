import React from 'react';
import Header from './Header';

const App = () => {
  return (
    <Header title={'My local header'} />
  );
};

export default App;
