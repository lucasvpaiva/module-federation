# Module Federation and Next

## Código baseado em [module-federation-examples](https://github.com/module-federation/module-federation-examples/tree/master/nextjs-ssr)

### Exemplo de uso da biblioteca [@telenko/node-mf](https://github.com/telenko/node-mf)

## Como executar


1.  Instalar dependências do `host`
    ```
    cd host
    npm i
    ```
2.  Instalar dependências do `header`
    ```
    cd header
    npm i
    ```
3.  Volte para pasta root e execute o projeto
    ```
    cd ..
    npm i
    npm start
    ```
4.  http://localhost:3000
